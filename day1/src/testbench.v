module day1_tb();
    reg clk;
    initial begin
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, day1_tb);
        clk = 0;
        forever begin
            #1;
            clk = ~clk;
        end
    end

    reg rst;
    initial begin
        rst = 1;
        #10;
        rst = 0;
    end

    wire valid;
    wire[15:0] out;

    initial begin
        #10000;
        $finish();
    end

    main uut(._i_clk(clk), ._i_rst(rst), .__output({valid, out}));
endmodule
